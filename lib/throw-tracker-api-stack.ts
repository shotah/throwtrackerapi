import {Construct} from 'constructs';
import {
  Duration,
  RemovalPolicy,
  Stack,
  StackProps,
  CfnOutput,
} from 'aws-cdk-lib';
import {
  aws_apigateway,
  aws_lambda_nodejs,
  aws_dynamodb,
  aws_logs,
  aws_lambda,
} from 'aws-cdk-lib';

export enum DeployEnvironment {
  TEST = 'test',
  DEV = 'dev',
  PROD = 'prod',
}

export class ThrowTrackerAPI extends Stack {
  constructor(
    scope: Construct,
    id: string,
    props?: StackProps,
    deployEnvironment: DeployEnvironment = DeployEnvironment.DEV
  ) {
    super(scope, id, props);

    const envVariables = {
      AWS_ACCOUNT_ID: Stack.of(this).account,
      POWERTOOLS_SERVICE_NAME: 'throw-tracker-api',
      POWERTOOLS_LOGGER_LOG_LEVEL: 'WARN',
      POWERTOOLS_LOGGER_SAMPLE_RATE: '0.01',
      POWERTOOLS_LOGGER_LOG_EVENT: 'true',
      POWERTOOLS_METRICS_NAMESPACE: 'throw-tracker',
      ENVIRONMENT: deployEnvironment,
    };

    /* ----------------- */
    /* Dynamodb table */
    /* ----------------- */
    const productsTable = new aws_dynamodb.Table(this, 'Products', {
      tableName: 'Products',
      partitionKey: {
        name: 'id',
        type: aws_dynamodb.AttributeType.STRING,
      },
      billingMode: aws_dynamodb.BillingMode.PAY_PER_REQUEST,
      removalPolicy: RemovalPolicy.DESTROY,
    });

    /* --------- */
    /* S3 Bucket */
    /* --------- */

    // const apiBucket = new s3.Bucket(this, 'throw-tracker-api');

    /* -------------------- */
    /* main lambda function */
    /* -------------------- */
    const throwTrackerFunction = new aws_lambda_nodejs.NodejsFunction(
      this,
      'ThrowTrackerFunction',
      {
        functionName: 'ThrowTrackerFunction',
        entry: './src/index.ts',
        handler: 'handler',
        awsSdkConnectionReuse: true,
        runtime: aws_lambda.Runtime.NODEJS_18_X,
        memorySize: 512,
        timeout: Duration.seconds(300),
        environment: {
          TABLE_NAME: productsTable.tableName,
          ...envVariables,
        },
        logRetention: aws_logs.RetentionDays.ONE_WEEK,
        tracing: aws_lambda.Tracing.ACTIVE,
        bundling: {
          minify: true,
        },
      }
    );

    /* --------------- */
    /* CloudWatch logs */
    /* --------------- */
    const logGroup = new aws_logs.LogGroup(this, 'ThrowTrackerLogGroup', {
      logGroupName: '/aws/aws_lambda/ThrowTrackerFunction',
      retention: aws_logs.RetentionDays.THREE_MONTHS,
      removalPolicy: RemovalPolicy.DESTROY,
    });

    /* ----------- */
    /* API Gateway */
    /* ----------- */
    const apiRestApi = new aws_apigateway.LambdaRestApi(this, 'ThrowTrackeraws_apigateway', {
      handler: throwTrackerFunction,
    });
  
    /* ----------------- */
    /* Outputs */
    /* ----------------- */
    new CfnOutput(this, 'ApiURL', {
      value: apiRestApi.url,
    });
  }
}
