import {Context, APIGatewayProxyEvent, APIGatewayProxyResult} from 'aws-lambda';
const api = require('lambda-api')();

api.get('/', (req: any, res: any) => {});

api.get('/course', async (req: any, res: any) => {
    return res.status(200).json({message: 'Hello World!'});
});

api.get('/throw', async (req: any, res: any) => {
  return res.status(200).json({message: 'Hello World!'});
});

api.get('/player', async (req: any, res: any) => {
  return res.status(200).json({message: 'Hello World!'});
});

export async function handler(event: APIGatewayProxyEvent, context: Context): Promise<APIGatewayProxyResult> {
  return await api.run(event, context);
}
