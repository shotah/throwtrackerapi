import * as cdk from 'aws-cdk-lib';
import {Template} from 'aws-cdk-lib/assertions';
import {
  ThrowTrackerAPI,
  DeployEnvironment,
} from '../lib/throw-tracker-api-stack';
import {describe} from 'mocha';

describe('Lambda and Permission Validation', () => {
  const app = new cdk.App();
  // WHEN
  const stack = new ThrowTrackerAPI(
    app,
    'MyTestStack',
    {},
    DeployEnvironment.TEST
  );
  // THEN
  const template = Template.fromStack(stack);
  console.log(template.toJSON());
  template.hasResourceProperties('AWS::Lambda::Function', {
    Timeout: 300,
  });
  // template.resourceCountIs('AWS::Lambda::Permission', 8);
});
