.SILENT: # Disable echo of commands
ifneq ("$(wildcard .env)","")
    include .env
endif

SHELL:=/bin/bash
export

all: install test

.PHONY : install
install :
	npm install
	npm run test

.PHONY: test
test:
	npm run test

.PHONY: coverage
coverage:
	npm run coverage

.PHONY: lint
lint:
	npm run lint

.PHONY: fix
fix:
	npm run fix

.PHONY: build
build: install
	npm run build

.PHONY: start
start: build
	npm run function


.PHONY: zip
zip: 
	npm run zip

.PHONY: upgrade-all
upgrade-all:
	npm i -g npm-check-updates
	npx ncu -u
	npm update


#####################
# SAM CLI Commands
#####################

# Start all APIs declared in the AWS CDK application
PHONY: sam-api
sam-api: build cdk-synth
	sam local start-api -t ./cdk.out/ThrowTrackerAPI.template.json --debug

# Start a local endpoint that emulates AWS Lambda to test the function
# https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-local-start-lambda.html
PHONY: sam-lambda
sam-lambda: build
	sam local start-lambda -t ./cdk.out/ThrowTrackerAPI.template.json --debug

# Invoke the function locally with a test event
PHONY: sam-event
sam-event: build
	sam local invoke -e event.json

# DEPRECATED
# PHONY: sam-package
# sam-package: build
# 	sam package \
# 	--template-file template.yaml \
# 	--output-template-file packaged.yaml \
# 	--s3-bucket $(S3_BUCKET)

# DEPRECATED
# PHONY: sam-deploy
# sam-deploy: s3-create 
# sam-deploy: sam-package
# 	sam deploy \
# 	--template-file packaged.yaml \
# 	--stack-name $(STACK_NAME) \
# 	--s3-bucket $(S3_BUCKET) \
# 	--capabilities CAPABILITY_IAM

# DEPRECATED
# PHONY: sam-destroy
# sam-destroy: s3-delete
# 	aws cloudformation delete-stack \
# 	--stack-name $(STACK_NAME)

# DEPRECATED
# PHONY: sam-logs
# sam-logs:
# 	sam logs -n $(FUNCTION_NAME) \
# 	--stack-name $(STACK_NAME) \
# 	--tail


#####################
# AWS CLI Commands
#####################

PHONY: validate-template
validate-template:
	aws cloudformation validate-template \
	--template-body file://template.yaml

PHONY: s3-create
s3-create:
	- aws s3api create-bucket \
	  --bucket $(S3_BUCKET) \
	  --region $(AWS_REGION) \
	  --acl private \
	  --create-bucket-configuration LocationConstraint=$(AWS_REGION)

PHONY: s3-empty
s3-empty:
	aws s3 rm s3://$(S3_BUCKET) --recursive

PHONY: s3-delete
s3-delete: s3-empty
	- aws s3api delete-bucket \
	  --bucket $(S3_BUCKET)


#####################
# CDK Commands
#####################

# # Welcome to your CDK TypeScript project

# You should explore the contents of this project. It demonstrates a CDK app with an instance of a stack (`SampleAppStack`)
# which contains an Amazon SQS queue that is subscribed to an Amazon SNS topic.

# The `cdk.json` file tells the CDK Toolkit how to execute your app.

# ## Useful commands

# * `npm run build`   compile typescript to js
# * `npm run watch`   watch for changes and compile
# * `npm run test`    perform the jest unit tests
# * `cdk deploy`      deploy this stack to your default AWS account/region
# * `cdk diff`        compare deployed stack with current state
# * `cdk synth`       emits the synthesized CloudFormation template

# initialize the CDK application -- only run once
PHONY: cdk-init
cdk-init:
	cdk init sample-app --language typescript

# install the CDK dependencies locally and sets up AWS account -- only run once
PHONY: cdk-bootstrap
cdk-bootstrap:
	cdk bootstrap

# Outputs the CloudFormation template for the CDK application
PHONY: cdk-synth
cdk-synth:
	cdk synth

# deploy the CDK application
PHONY: cdk-deploy
cdk-deploy:
	cdk deploy

# destroy the CDK application
PHONY: cdk-destroy
cdk-destroy:
	cdk destroy

# list the CDK application
PHONY: cdk-list
cdk-list:
	cdk list

# diff the CDK application
PHONY: cdk-diff
cdk-diff:
	cdk diff

# context the CDK application
PHONY: cdk-context
cdk-context:
	cdk context

# version the CDK application
PHONY: cdk-version
cdk-version:
	cdk version
