# Throw Tracker API

## Description

This is the API for the Throw Tracker application. It is built using Typescript, Lambda, and DynamoDB.

## Getting Started

### Dependencies

- Node
- NPM
- docker
- aws cli
- aws sam cli
- aws cdk

### Installing

- Clone the repo
- Create .env file in the root directory
  - Add `STACK_NAME=<your stack name>`
  - Add `AWS_ACCESS_KEY_ID=<your aws access key id>`
  - Add `AWS_SECRET_ACCESS_KEY=<your aws secret access key>`
  - Add `AWS_REGION=<your aws region>`
  - Add `S3_BUCKET=<your s3 bucket name>`
- Run `make install`
- Run `make test` to run the tests

### Test locally

link to aws sam cli docs <https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-cdk-testing.html>

```bash
make cdk-synth
make sam-api

curl http://127.0.0.1:3000/throw
```
