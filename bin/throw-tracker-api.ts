#!/usr/bin/env node
import * as cdk from 'aws-cdk-lib';
import {
  ThrowTrackerAPI,
  DeployEnvironment,
} from '../lib/throw-tracker-api-stack';

const app = new cdk.App();
new ThrowTrackerAPI(
  app,
  'ThrowTrackerAPI',
  {
    stackName: 'ThrowTrackerAPI',
    env: {
      account: process.env.CDK_DEFAULT_ACCOUNT,
      region: process.env.CDK_DEFAULT_REGION,
    },
    terminationProtection: false,
  },
  DeployEnvironment.DEV
);
